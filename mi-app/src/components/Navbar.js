import React from 'react'
import { Link} from 'react-router-dom'
const Navbar = () => {
  return (
    <>
    <nav className="navbar navbar-expand-lg navbar-light bg-ligt">
  <div className="container-fluid">
    <Link to='/inicio'>
    <img src='./logo1.png' width='70'/>
    </Link>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarNavDropdown">
      <ul className="navbar-nav mx-5">
        <li className="nav-item">
          <Link className="nav-link active" aria-current="page" to="/inicio"></Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/asistencia">Asistencias</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/destacados">Destacados</Link>
        </li>
        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Opciones
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <li><Link className="dropdown-item" to="/formulario">Registro</Link></li>
            <li><Link className="dropdown-item" to="/iniciar">Iniciar Sesión</Link></li>
            
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
    </>
  )
}

export default Navbar