import React, { useState } from 'react';

const Contacto = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!validateEmail(email)) {
      setMessage('Ingresa un correo electrónico válido');
      return;
    }

    setMessage('Sesión iniciada');
  };

  const validateEmail = (input) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(input);
  };

  return (
    <div className='fondo'>
    <div className="container">
      <h1>Inicie Sesión</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="email">Correo Electrónico:</label>
          <input
            type="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </div>

        <div className="form-group">
          <label htmlFor="password">Contraseña:</label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </div>

        <button type="submit" className="btn">Iniciar Sesión</button>
      </form>

      {message && <p className="message">{message}</p>}
    </div>
    <div>
    <div className="footer">
          <div className="break">
              <div class="direccion">
                  <h4>Dirección:</h4>
                  <p>Av. Circunvalacion, Manta</p>
              </div>
              <div class="contacto">
                  <h4>Contactos:</h4>
                  <p>Tel: +593 998544663</p>
                  <p>Email: Uleam@gamil.com</p>
              </div>
              <div class="sobre">
                  <h4>Sobre nosotros:</h4>
                  <p>Somos un hotel de calidad y profesionalismo. Contamos con habitaciones equipadas para ofrecer confort.</p>
              </div>
          </div>
          <div class="derechos">
              <h4> ULEAM @ 2023</h4>
          </div>
         
      </div>
    </div>
    <p>.</p>
    <br></br>
    <p>.</p>
    <br></br>
    <p>.</p>
    <br></br>
    <p>.</p>
    <br></br>
    <p>.</p>
    <br></br>
    <p>.</p>
    <br></br>
    <p>.</p>
    <br></br>
    </div>
  );
};

export default Contacto;