import React, { useState } from 'react';

const FormularioTabla = () => {
  const [formData, setFormData] = useState({
    nombre: '',
    apellido: '',
    edad: '',
    telefono: '',
    correo: '',
    contraseña: '',
    confirmarContraseña: ''
  });

  const [tablaData, setTablaData] = useState([]);
  const [mensajeError, setMensajeError] = useState('');

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (formData.contraseña !== formData.confirmarContraseña) {
      setMensajeError('Las contraseñas no coinciden');
      return;
    }

    setTablaData([...tablaData, formData]);
    setFormData({
      nombre: '',
      apellido: '',
      edad: '',
      telefono: '',
      correo: '',
      contraseña: '',
      confirmarContraseña: ''
    });
    setMensajeError('');
  };

  return (
    <div className='fondo'>
    <div className="container">
      
      <h1>Formulario en una tabla</h1>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="nombre">Nombre:</label>
          <input
            type="text"
            id="nombre"
            name="nombre"
            value={formData.nombre}
            onChange={handleChange}
            maxLength={30}
            required
          />
        </div>

        <div className="form-group">
          <label htmlFor="apellido">Apellido:</label>
          <input
            type="text"
            id="apellido"
            name="apellido"
            value={formData.apellido}
            onChange={handleChange}
            maxLength={30}
            required
          />
        </div>

        <div className="form-group">
          <label htmlFor="edad">Edad:</label>
          <input
            type="number"
            id="edad"
            name="edad"
            value={formData.edad}
            onChange={handleChange}
            min={0}
            max={110}
            required
          />
        </div>

        <div className="form-group">
          <label htmlFor="telefono">Número de teléfono:</label>
          <input
            type="text"
            id="telefono"
            name="telefono"
            value={formData.telefono}
            onChange={handleChange}
            maxLength={10}
            required
          />
        </div>

        <div className="form-group">
          <label htmlFor="correo">Correo electrónico:</label>
          <input
            type="email"
            id="correo"
            name="correo"
            value={formData.correo}
            onChange={handleChange}
            required
          />
        </div>

        <div className="form-group">
          <label htmlFor="contraseña">Contraseña:</label>
          <input
            type="password"
            id="contraseña"
            name="contraseña"
            value={formData.contraseña}
            onChange={handleChange}
            required
          />
        </div>

        <div className="form-group">
          <label htmlFor="confirmarContraseña">Confirmar contraseña:</label>
          <input
            type="password"
            id="confirmarContraseña"
            name="confirmarContraseña"
            value={formData.confirmarContraseña}
            onChange={handleChange}
            required
          />
        </div>
        

        {mensajeError && <p className="error">{mensajeError}</p>}

        <button type="submit" className="btn">Registrarse</button>
      </form>

      <table className="tabla">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Edad</th>
            <th>Teléfono</th>
            <th>Correo</th>
          </tr>
        </thead>
        <tbody>
          {tablaData.map((item, index) => (
            <tr key={index}>
              <td>{item.nombre}</td>
              <td>{item.apellido}</td>
              <td>{item.edad}</td>
              <td>{item.telefono}</td>
              <td>{item.correo}</td>
            </tr>
          ))}
        </tbody>
      </table>
      
    </div>
  
    </div>
  );
};

export default FormularioTabla;
