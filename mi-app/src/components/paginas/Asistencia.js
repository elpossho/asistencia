import React, { useState } from 'react';


const Asistencia = () => {
  
  const [fecha, setFecha] = useState('');
  const [asistenciaConfirmada, setAsistenciaConfirmada] = useState(false);

  const handleFechaChange = (event) => {
    setFecha(event.target.value);
  };

  const handleAsistenciaConfirmada = () => {
    setAsistenciaConfirmada(true);
  };

  return (
    <div className='fondo'>
    <div>
      <div className='asis'>
      <h2>Asistencia</h2>
      
      
      <table className="fecha-table">
        
        <tbody>
          <tr>
            <td>
              <label htmlFor="fecha">Seleccionar fecha:</label>
            </td>
            <td>
              <input type="date" id="fecha" value={fecha} onChange={handleFechaChange} />
            </td>
          </tr>
        </tbody>
        
      </table>
      
      
      

      <h3>Lista de asistencia:</h3>
      </div>
      <table className="asistencia-table">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Marque Inasistencia</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Juan Angel Abreu</td>
            <td>
              <input type="checkbox" />
            </td>
          </tr>
          <tr>
            <td>Beñat Gómez</td>
            <td>
              <input type="checkbox" />
            </td>
          </tr>
          <tr>
            <td>Octavio Nevado</td>
            <td>
              <input type="checkbox" />
            </td>
          </tr>
          <tr>
            <td>Albert Salgado</td>
            <td>
              <input type="checkbox" />
            </td>
          </tr>
          <tr>
            <td>Soledad Hoyos</td>
            <td>
              <input type="checkbox" />
            </td>
          </tr>
          <tr>
            <td>Fermina Costas</td>
            <td>
              <input type="checkbox" />
            </td>
          </tr>
          <tr>
            <td>Mireya Carretero</td>
            <td>
              <input type="checkbox" />
            </td>
          </tr>
          <tr>
            <td>Asier Matas</td>
            <td>
              <input type="checkbox" />
            </td>
          </tr>
          <tr>
            <td>Higinio Carranza</td>
            <td>
              <input type="checkbox" />
            </td>
          </tr>
          <tr>
            <td>Antoni Ye</td>
            <td>
              <input type="checkbox" />
            </td>
          </tr>
          <tr>
            <td>Youssef Galiano</td>
            <td>
              <input type="checkbox" />
            </td>
          </tr>
          <tr>
            <td>Isaac Jaime</td>
            <td>
              <input type="checkbox" />
            </td>
          </tr>
          <tr>
            <td>Guadalupe Hermoso</td>
            <td>
              <input type="checkbox" />
            </td>
          </tr>
        </tbody>
      </table>

      <button onClick={handleAsistenciaConfirmada}>Confirmar asistencia</button>

      {asistenciaConfirmada && <p>¡Asistencia confirmada!</p>}
    </div>
    </div>
  );
};

export default Asistencia;