import React from 'react'
import imagen3 from '../imagenes/img3.jpg'
import imagen4 from '../imagenes/img4.jpg'
import imagen5 from '../imagenes/img5.jpg'
const Destacados = () => {
  return (
    
       <div className="destacados">
        
      <h2>Destacados</h2>
      <table className="tabla-destacados">
        <tbody>
          <tr>
            <td>
            <img src={imagen3} alt="Descripción de la imagen" />
              <p>Frace del día</p>
            </td>
            <td>
            <img src={imagen4} alt="Descripción de la imagen" />
              <p>Fenómeno del Niño</p>
            </td>
            <td>
            <img src={imagen5} alt="Descripción de la imagen" />
              <p>Calendario de Homologaciones</p>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    
    
  )
}

export default Destacados