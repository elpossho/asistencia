import React from 'react';
import imagen from '../imagenes/img2.jpg'

const Inicio = () => {
  return (
    <div className="fondo">
    <h1 className='eloy'>Universidad Laica Eloy Alfaro de Manabí</h1>
    <div className="uni">
      <img src={imagen} alt="Descripción de la imagen" />
    </div>
    <div className='parrado'> 
    <p>
      La Universidad Laica Eloy Alfaro de Manabí es una institución educativa ubicada en la provincia de Manabí, Ecuador.
      Fundada en XXXX, la universidad se dedica a la formación de profesionales en diversas áreas del conocimiento.
    </p>
    <div className="footer">
          <div className="break">
              <div class="direccion">
                  <h4>Dirección:</h4>
                  <p>Av. Circunvalacion, Manta</p>
              </div>
              <div class="contacto">
                  <h4>Contactos:</h4>
                  <p>Tel: +593 998544663</p>
                  <p>Email: Uleam@gamil.com</p>
              </div>
              <div class="sobre">
                  <h4>Sobre nosotros:</h4>
                  <p>Somos un hotel de calidad y profesionalismo. Contamos con habitaciones equipadas para ofrecer confort.</p>
              </div>
          </div>
          <div class="derechos">
              <h4> ULEAM @ 2023</h4>
          </div>
         
      </div>
    </div>
        
  </div>
  );
};

export default Inicio;