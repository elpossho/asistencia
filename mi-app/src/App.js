import './App.css';
import { Route, Routes, BrowserRouter } from 'react-router-dom';
import Navbar from './components/Navbar';


import Inicio from './components/paginas/Inicio';
import Destacados from './components/paginas/Destacados';
import FormularioTabla from './components/paginas/FormularioTabla';
import Iniciar from './components/paginas/Iniciar';
import Asistencia from './components/paginas/Asistencia'



function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Navbar/>
      <Routes>
        <Route path='/inicio' element={<Inicio/>}/>
        <Route path='/iniciar' element={<Iniciar/>}/>
        <Route path='/destacados' element={<Destacados/>}/>
        
        <Route path='/formulario' element={<FormularioTabla/>}/>
        <Route path='/asistencia' element={<Asistencia/>}/>
      </Routes>
    </BrowserRouter>
    </div>
    
  );
}

export default App;
